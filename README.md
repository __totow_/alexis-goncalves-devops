# Alexis Goncalves - DevOps

## Note base de la mise a niveau

1. Créer un Réseau Personnalisé

-gcloud compute networks create taw-custom-network --subnet-mode custom

2. Créer un Sous-Réseau dans la Région Europe-West1 et autres régions

-gcloud compute networks subnets create subnet-us-east4 \
   --network taw-custom-network \
   --region us-east4 \
   --range 10.0.0.0/16
-gcloud compute networks subnets create subnet-europe-west4 \
   --network taw-custom-network \
   --region europe-west4 \
   --range 10.1.0.0/16
-gcloud compute networks subnets create subnet-us-east1 \
   --network taw-custom-network \
   --region us-east1 \
   --range 10.2.0.0/16
-gcloud compute networks subnets list \
   --network taw-custom-network

3. Créer une règle autorisant le trafic HTTP, icmp, ssh et rdp

-gcloud compute firewall-rules create nw101-allow-http \
   --allow tcp:80 --network taw-custom-network --source-ranges 0.0.0.0/0 \
   --target-tags http
-gcloud compute firewall-rules create "nw101-allow-icmp" --allow icmp --network "taw-custom-network" --target-tags rules
-gcloud compute firewall-rules create "nw101-allow-internal" --
   allow tcp:0-65535,udp:0-65535,icmp --network "taw-custom-network"     
   --source-ranges "10.0.0.0/16","10.2.0.0/16","10.1.0.0/16"
-gcloud compute firewall-rules create "nw101-allow-ssh" --allow 
   tcp:22 --network "taw-custom-network" --target-tags "ssh"
-gcloud compute firewall-rules create "nw101-allow-rdp" --allow tcp:3389 --network "taw-custom-network"

4. Créer une instance dans la zone Europe-West1-D et autres régions

-gcloud compute instances create us-test-01 \
   --subnet subnet-us-east4 \
   --zone us-east4-a \
   --machine-type e2-standard-2 \
   --tags ssh,http,rules
-gcloud compute instances create us-test-02 \
   --subnet subnet-europe-west4 \
   --zone europe-west4-c \
   --machine-type e2-standard-2 \
   --tags ssh,http,rules
-gcloud compute instances create us-test-03 \
   --subnet subnet-us-east1 \
   --zone us-east1-b \
   --machine-type e2-standard-2 \
   --tags ssh,http,rules

5. Vérifications et tests

-ping -c 3 34.32.182.216
-ping -c 3 35.227.37.189
-ping -c 3 us-test-02.europe-west4-c
-sudo apt-get update (sur le us-test-01)
sudo apt-get -y install traceroute mtr tcpdump iperf whois host dnsutils siege
-traceroute www.icann.org
-sudo apt-get update (sur le us-test-02)
sudo apt-get -y install traceroute mtr tcpdump iperf whois host dnsutils siege
-iperf -s #run in server mode (sur le us-test-01)
-iperf -c us-test-01.us-east4-a #run in client mode (sur le us-test-02)

## Principe de base de terraform

1. Vérifiez si Terraform est disponible

Terraform

2. Créer et modifier le fichier instance.trafic

touch instance.tf
nano instance.tf

3. Contenu du fichier :

resource "google_compute_instance" "terraform" {
  project      = "qwiklabs-gcp-01-90d651f2a479"
  name         = "terraform"
  machine_type = "e2-medium"
  zone         = "us-central1-a"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
}

4. Téléchargez et installez le binaire

terraform init

5. Créez un plan d'exécution

terraform plan

6. Appliquer les modifications

terraform apply
